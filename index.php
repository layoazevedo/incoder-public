<?php
require "includes/db/conn.php";
require "includes/header.php";
require "includes/menu-superior.php";
require "includes/menu-lateral.php";

?>

<!-- MENU CONTEÚDO -->
<section class="conteudo">
    <div class="container">
        <div id="conteudo" class="conteudo padding-left-255">
            <div class="pesquisa">
                <div class="nav pull-right top-menu">
                    <input class="form-control busca" placeholder="Buscar" type="text" id="buscaIncoder" onkeyup="buscaDinamica()">
                </div>
            </div>

            <div class="row">
                <ul id="filtro">
                    <?php
                        $cadGeral = mysqli_query($conn, "SELECT * FROM Tab_CadGeral WHERE STATUS='ATIVO' ORDER BY ID_EFETIVO DESC");

                        while ($obj = $cadGeral->fetch_object()) {
                            echo '
                                <li style="color: white;"><a href="profile.php?account='.$obj->ID_EFETIVO.'"><div class="col-md-4">
                                    <div class="grid">
                                        <div class="mini-perfil">
                                            <div class="nome">'.utf8_encode($obj->NOME_COLABORADOR).'</div>';

                                            if (empty($obj->CAMINHO_FOTO)) {
                                                echo '
                                                <div class="img-perfil">
                                                    <img src="http://gcsportoseguro.com.br/photos/sem-foto.jpg" style="width: 126px;">
                                                </div>';
                                            } else {
                                                echo '
                                                <div class="img-perfil">
                                                    <img src="http://gcsportoseguro.com.br/'.$obj->CAMINHO_FOTO.'" style="width: 126px;">
                                                </div>';
                                            }

                                            echo '
                                            <div class="desc">
                                                <span>Address</span>
                                                <p>';

                                                if (empty($obj->ENDERECO_RESIDENCIAL)) {
                                                    echo 'N/D'.'<br />';
                                                } else {
                                                    echo utf8_encode($obj->ENDERECO_RESIDENCIAL).'<br />';
                                                }

                                                if (empty($obj->BAIRRO)) {
                                                    echo 'N/D'.'<br />';
                                                } else {
                                                    echo utf8_encode($obj->BAIRRO).'<br />';
                                                }

                                                if (empty($obj->CEP)) {
                                                    echo 'N/D'.'<br />';
                                                } else {
                                                    echo utf8_encode($obj->CEP).'<br />';
                                                }

                                                echo '</p>

                                                <span>Phone</span>';

                                                if (empty($obj->TELEFONE_RESIDENCIAL)) {
                                                    echo 'N/D'.'<br />';
                                                } else {
                                                    echo '<p>'.$obj->TELEFONE_RESIDENCIAL.'</p>';
                                                }

                                                if(empty($obj->E_MAIL)) {
                                                    echo 'N/D'.'<br />';
                                                } else {
                                                    echo '<span>E-mail</span>
                                                        '.utf8_encode($obj->E_MAIL).'
                                                    </div>';
                                                }

                                            echo '</div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            ';
                        }

                        require "includes/db/close-conn.php";
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- BUSCA DINAMICA -->
<script>
    function buscaDinamica() {
        var input, filter, ul, li, a, i;
        input = document.getElementById('buscaIncoder');
        filter = input.value.toUpperCase();
        ul = document.getElementById("filtro");
        li = ul.getElementsByTagName('li');

        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
</script>

<!-- JQUERY SIDEBAR E TOOLTIP -->
<script>
    $.sidebarMenu($('.sidebar-menu'));

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script>
$(document).ready(function(){
    $(".toggle").click(function(){
        $(".menu-lateral").toggle('slide',200);
    });
    $(".toggle").click(function(){
        $("#conteudo").toggleClass('padding-left-235 padding-left-0');
    });
});
</script>

<script type="text/javascript">
    var wr = new Optiscroll(document.getElementById('m-wrapper'), { forceScrollbars: true });
    var wr = new Optiscroll(document.getElementById('m-wrapper-top'), { forceScrollbars: true });
</script>
</body>
</html>
