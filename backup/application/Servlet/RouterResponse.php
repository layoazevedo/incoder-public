<?php

namespace SistemaPro\Servlet;

class RouterResponse
{
    private $router;

    /**
     * @method   [setRouterHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Servlet] [setters]
     * @version  [1.0.2]
     * @param    [$router]
     * @return   [void]
     */

    public function setRouterHttp($router)
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @method   [getRouter]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Servlet] [getters]
     * @version  [1.0.2]
     * @return   [Array]
     */

    public function getRouter($index)
    {
        $request = require dirname(__FILE__) . '/../Config/request.config.php';

        if (isset($this->router[$request['requestConfig'][$index]])) {
            return $this->router[$request['requestConfig'][$index]];
        }
        
        return false;
    }
}
