<?php

namespace SistemaPro\Servlet;

class GetRequest
{
    private $get;

    /**
     * @method   [setGetHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Servlet] [setters]
     * @version  [1.0.2]
     * @param    [$get]
     * @return   [void]
     */

    public function setGetHttp($get)
    {
        $this->get = $get;
        return $this;
    }

    /**
     * @method   [getHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Servlet] [getters]
     * @version  [1.0.2]
     * @return   [Array]
     */

    public function getHttp($index)
    {
        if (isset ($this->get[$index])) {
            return $this->get[$index];
        }

        return 0;
    }

    /**
     * @method   [getHttpRequest]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-04-04]
     * @category [Servlet] [getters]
     * @version  [1.0.2]
     * @return   [Array]
     */

    public function getHttpRequest($value)
    {
        return isset($_GET[$value]) ? $_GET[$value] : '';
    }

    /**
     * @method   [isEmpty] [Verifica se a classe está vazia]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-04-04]
     * @category [Servlet]
     * @version  [1.0.2]
     * @return   [bool]
     */

    public function isEmpty()
    {
        if (count($this->get) > 0) {
            return false;
        }
        return true;
    }
}
