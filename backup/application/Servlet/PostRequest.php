<?php

namespace SistemaPro\Servlet;

class PostRequest
{
    private $post;

    /**
     * @method   [setPostHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Servlet] [setters]
     * @version  [1.0.2]
     * @param    [$post]
     * @return   [void]
     */

    public function setPostHttp($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @method   [getPostHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Servlet] [getters]
     * @version  [1.0.2]
     * @return   [Array]
     */

    public function getPostHttp($index)
    {
        if (isset ($this->post[$index])) {
            return $this->post[$index];
        }

        return 0;
    }
}
