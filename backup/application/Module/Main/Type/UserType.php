<?php

namespace SistemaPro\Module\Main\Type;

interface UserType
{
    /**
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Interfaces]
     * @since    [2017-06-07]
     * @category [Interface]
     * @version  [1.0.2]
     */

    const USER        = 'User';
    const USER_BY_ID  = 'UserById';
    const TREINAMENTO = 'Treinamento';
    const NOTAS       = 'Notas';
}
