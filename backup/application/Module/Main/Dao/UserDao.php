<?php

namespace SistemaPro\Module\Main\Dao;

use SistemaPro\Core\Dao;

class UserDao extends Dao
{
    /**
     * @method   [getTreimentoById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Dao]
     * @param    int $idUser
     * @return   array
     * @version  [1.0.2]
     */
    public function getTreimentoById($idUser)
    {
        $this->setTable('Tab_Treinamentos');
        $this->setQuery(
            $this->query("
                SELECT
                    DATE(CADASTRO) as CADASTRO,
                    EXAM_ONLINE,
                    CONC_PERC,
                    TIPO_CURSO,
                    NOME_CURSO
                FROM
                    Tab_Treinamentos
                WHERE
                    ID_EFETIVO = '".$idUser."'"
                )
            );

        $this->setField(
            array(
                'cadastro'    => 'CADASTRO',
                'exameOnline' => 'EXAM_ONLINE',
                'percurso'    => 'CONC_PERC',
                'tipoCurso'   => 'TIPO_CURSO',
                'nomeCurso'   => 'NOME_CURSO',
            )
        );

        /*$this->setQuery(
            $this->query("
                SELECT
                    t.ID_EFETIVO as ID_EFETIVO,
                    n.MEDIA * 10 as MEDIA,
                    t.TIPO as TIPO,
                    ep.STATUS_AA as STATUS_AA,
                    e.DATA_PROG as DATA_PROG
                FROM
                    Tab_Treinamentos t
                INNER JOIN
                    Tab_EntrevProgr ep ON t.ID_EFETIVO = ep.ID_EFETIVO
                INNER JOIN
                    Tab_EntrevNotas n ON n.ID_EFETIVO = ep.ID_EFETIVO
                INNER JOIN
                    Tab_EntrevProgr e ON e.ID_EFETIVO = ep.ID_EFETIVO
                    WHERE
                        t.ID_EFETIVO = '".$idUser."'
                    "
                )
            );

        $this->setField(
            array(
                'idEfetivo' => 'ID_EFETIVO',
                'media'     => 'MEDIA',
                'tipo'      => 'TIPO',
                'status'    => 'STATUS_AA',
                'dataProg'  => 'DATA_PROG',
            )
        ); */

        return $this->fetchAll($this->getQuery(), $this->getField());
    }

    /**
     * @method   [getUserById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Dao]
     * @param    int $idUser
     * @return   array
     * @version  [1.0.2]
     */
    public function getNotasById($idUser)
    {
        $this->setTable('Tab_EntrevNotas');
        $this->setQuery(
            $this->query("SELECT * FROM Tab_EntrevNotas where ID_EFETIVO = '".$idUser."'")
        );
        $this->setField(
            array(
                'idEfetivo'           => 'ID_EFETIVO',
                'media'     => 'MEDIA',
                'tipo'     => 'Tipo',
            )
        );

        return $this->fetchAll($this->getQuery(), $this->getField());
    }

    /**
     * @method   [getUserById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Dao]
     * @param    int $idUser
     * @return   array
     * @version  [1.0.2]
     */
    public function getUserById($idUser)
    {
        $this->setTable('Tab_CadGeral');
        $this->setQuery(
            $this->query("SELECT * FROM Tab_CadGeral where ID_EFETIVO = '".$idUser."'")
        );
        $this->setField(
            array(
                'idEfetivo'           => 'ID_EFETIVO',
                'nomeColaborador'     => 'NOME_COLABORADOR',
                'enderecoResidencial' => 'ENDERECO_RESIDENCIAL',
                'bairro'              => 'BAIRRO',
                'cep'                 => 'CEP',
                'telefoneResidencial' => 'TELEFONE_RESIDENCIAL',
                'telefoneCelular'     => 'TELEFONE_CELULAR',
                'email'               => 'E_MAIL',
                'foto'                => 'CAMINHO_FOTO',
                'uf'                  => 'UF_NASC',
                'cidade'              => 'CIDADE_NASC',
                'cargo'               => 'CARGO',
                'status'              => 'STATUS',
                'dataAdmissao'        => 'DATA_DE_ADMISSAO',
                'matricula'           => 'MATRICULA',
            )
        );

        return $this->fetchAll($this->getQuery(), $this->getField());
    }

    /**
     * @method   [getAll]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-07]
     * @category [Dao]
     * @return   array
     * @version  [1.0.2]
     */
    public function getAll()
    {
        $this->setTable('Tab_CadGeral');
        $this->setQuery(
            $this->query("SELECT * FROM Tab_CadGeral WHERE STATUS='ATIVO' ORDER BY ID_EFETIVO DESC")
        );
        $this->setField(
            array(
                'idEfetivo'           => 'ID_EFETIVO',
                'nomeColaborador'     => 'NOME_COLABORADOR',
                'enderecoResidencial' => 'ENDERECO_RESIDENCIAL',
                'bairro'              => 'BAIRRO',
                'cep'                 => 'CEP',
                'telefoneResidencial' => 'TELEFONE_RESIDENCIAL',
                'email'               => 'E_MAIL',
                'foto'                => 'CAMINHO_FOTO',
            )
        );

        return $this->fetchAll($this->getQuery(), $this->getField());
    }
}
