<?php

namespace SistemaPro\Module\Main\Controller;

use SistemaPro\Module\Main\Service\UserService;
use SistemaPro\Module\Main\Type\UserType;
use SistemaPro\Core\ActionController;

class HomeController extends ActionController
{
    public function __construct()
    {
        $this->userService = new UserService();
    }

    /**
     * @method   [indexAction]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-07]
     * @category [Controller]
     * @version  [1.0.2]
     */
    public function indexAction()
    {
        $this->setTemplate(true);
        $this->view('index',
            array(
                UserType::USER => $this->userService->getUserAll()->get(UserType::USER)
            )
        );
    }
}
