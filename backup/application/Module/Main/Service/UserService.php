<?php

namespace SistemaPro\Module\Main\Service;

use SistemaPro\Core\Service;
use SistemaPro\Module\Main\Dao\UserDao;
use SistemaPro\Module\Main\Type\UserType;

class UserService extends Service
{
    private $query;

    /**
     * @method   [getTreimentoById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Service]
     * @param    $idUser
     * @return   Object
     * @version  [1.0.2]
     */
    public function getNotasById($idUser)
    {
        $this->query = (new UserDao())->getNotasById($idUser);

        $this->getServiceLocator()->setService(
             UserType::NOTAS,
             $this->query
        );

        return $this->getServiceLocator();
    }

    /**
     * @method   [getTreimentoById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Service]
     * @param    $idUser
     * @return   Object
     * @version  [1.0.2]
     */
    public function getTreimentoById($idUser)
    {
        $this->query = (new UserDao())->getTreimentoById($idUser);

        $this->getServiceLocator()->setService(
             UserType::TREINAMENTO,
             $this->query
        );

        return $this->getServiceLocator();
    }

    /**
     * @method   [getUserById]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-13]
     * @category [Service]
     * @param    $idUser
     * @return   Object
     * @version  [1.0.2]
     */
    public function getUserById($idUser)
    {
        $this->query = (new UserDao())->getUserById($idUser);

        $this->getServiceLocator()->setService(
             UserType::USER_BY_ID,
             $this->query
        );

        return $this->getServiceLocator();
    }

    /**
     * @method   [getUserAll]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-06-07]
     * @category [Service]
     * @return   Object
     * @version  [1.0.2]
     */
    public function getUserAll()
    {
        $this->query = (new UserDao())->getAll();

        $this->getServiceLocator()->setService(
             UserType::USER,
             $this->query
        );

        return $this->getServiceLocator();
    }
}
