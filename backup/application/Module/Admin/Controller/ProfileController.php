<?php

namespace SistemaPro\Module\Admin\Controller;

use SistemaPro\Module\Main\Service\UserService;
use SistemaPro\Module\Main\Type\UserType;
use SistemaPro\Core\ActionController;

class ProfileController extends ActionController
{
    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function userAction()
    {
        $this->setTemplate(true);
        $this->view('profile',
            array(
                UserType::USER_BY_ID  => $this->userService->getUserById($_GET['account'])
                    ->get(UserType::USER_BY_ID)[0],
                UserType::TREINAMENTO => $this->userService->getTreimentoById($_GET['account'])
                    ->get(UserType::TREINAMENTO),
                UserType::NOTAS => $this->userService->getNotasById($_GET['account'])
                    ->get(UserType::NOTAS)
            )
        );
    }
}
