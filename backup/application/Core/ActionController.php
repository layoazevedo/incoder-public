<?php

namespace SistemaPro\Core;

use SistemaPro\Core\Abstracts\ControllerAbstract;
use SistemaPro\Core\Interfaces\ServiceInterface;
use Zend\ServiceManager\ServiceManager;
use SistemaPro\Servlet\RouterResponse;
use SistemaPro\Servlet\PostRequest;
use SistemaPro\Servlet\GetRequest;

class ActionController extends ControllerAbstract
{
    private $envConfig = 'default';
    private $view;
    private $template;
    protected $paramUrl;
    protected $get;
    protected $post;
    protected $serviceManager;

    /**
     * @method   [init] [Inicia a instancia do twig para o framework]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-07-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @see      SistemaPro\Core\Router
     * @return   [object]
     */

    public function init()
    {
        $this->serviceManager = new ServiceManager();

        require_once dirname(__FILE__) . '/../../vendor/twig/twig/lib/Twig/Autoloader.php';

        $loader = new \Twig_Loader_Filesystem(dirname(__FILE__) . '/../View');
        $this->view = new \Twig_Environment($loader, array(
            'nocache' => '/var/tmp/twig',
        ));

        return $this;
    }

    /**
     * @method   [getServiceLocator] [Retorna o service locator do ZF2]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-17-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     */

    public function getServiceLocator() {
         return $this->serviceManager;
    }

    /**
     * @method   [view] [Verifica o status da template para renderização na view]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-07-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [$template, $dados]
     * @return   [void]
     */

    public function view($template, $dados)
    {
        $viewRouter = $this->getRouter()->getModule().'/'.$template;

        if ($this->getTemplate() == true) {
            $this->getLayout('header', $dados);
            $this->getRenderTwig($viewRouter, $dados);
            $this->getLayout('footer', $dados);
        } else {
            $this->getRenderTwig($template, $dados);
        }
    }

    /**
     * @method   [getRenderTwig]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-08-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [$template, $dados]
     * @return   [void]
     */

    private function getRenderTwig($template, $dados)
    {
        $configPadrao = array(
            ServiceInterface::AMBIENTE => $this->getUrlEnv()->getAmbiente(),
            ServiceInterface::PAGINA   => $this->envConfig,
        );

        $result = array_merge($configPadrao, $dados);

        echo $this->view->render($template.'.html', $result);
    }

    /**
     * @method   [setTemplate]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-08-03]
     * @category [Controller] [setters]
     * @version  [1.0.2]
     * @param    [$template]
     * @return   [bool]
     */

    public function setTemplate($template = true)
    {
        $this->template = $template;
        return $template;
    }

    /**
     * @method   [getTemplate]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-08-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @return   [bool]
     */

    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @method   [getLayout]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-08-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [$layout]
     * @return   [String]
     */

    private function getLayout($layout, $dados)
    {
        $view = 'Layout/'.$layout;

        try {
            $this->getRenderTwig($view, $dados);
        } catch (\Exception $erro) {
            var_dump($erro->getMessage());
        }
    }

    /**
     * @method   [getHelperAdsense]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-22-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     */

    protected function getHelperAdsense()
    {
        return $this->getHelper()->get();
    }

    /**
     * @method   [getHelperMenu]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-22-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [$pagina, $atual]
     * @return   [Object]
     */

    protected function getHelperMenu($pagina, $atual)
    {
        return $this->getHelper()->getMenu($pagina, $atual);
    }

    /**
     * @method   [setRouterHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [setters]
     * @version  [1.0.2]
     * @param    [$paramUrl]
     * @return   [Object]
     */

    public function setRouterHttp(RouterResponse $paramUrl)
    {
        $this->paramUrl = $paramUrl;
        return $this;
    }

    /**
     * @method   [getRouterHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     */

    public function getRouterHttp()
    {
        return $this->paramUrl;
    }

    /**
     * @method   [setPost]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [setters]
     * @version  [1.0.2]
     * @param    [$post]
     * @return   [Object]
     */

    public function setPostRequestHttp(PostRequest $post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @method   [getPost]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     */

    public function getPostRequestHttp()
    {
        return $this->post;
    }

    /**
     * @method   [setGetHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [setters]
     * @version  [1.0.2]
     * @param    [$get]
     * @return   [Object]
     */

    public function setGetRequestHttp(GetRequest $get)
    {
        $this->get = $get;
        return $this;
    }

    /**
     * @method   [getRequestHttp]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-29-03]
     * @category [Controller] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     */

    public function getRequestHttp()
    {
        return $this->get;
    }

    /**
     * @method   [factoryPage] [Factory para template de erros]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-19-04]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [$page]
     * @return   [object]
     */

    private function factoryPage($page)
    {
        $this->getLayout('header', array(
                ServiceInterface::ADSENSE_CLIENT => $this->getHelperAdsense()['ad-client'],
                ServiceInterface::ADSENSE_SLOT   => $this->getHelperAdsense()['ad-slot'],
                ServiceInterface::AMBIENTE       => $this->getUrlEnv()->getAmbiente()
            )
        );
        $this->getLayout($page, array(ServiceInterface::AMBIENTE => $this->getUrlEnv()->getAmbiente()));
        $this->getLayout('footer', array());
    }

    /**
     * @method   [pageNotFound] [Retorna pagina de erro]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-30-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [object]
     */

    public function pageNotFound()
    {
        $this->factoryPage('error/erro404');
    }

    /**
     * @method   [queryNotFound] [Retorna buscas vazias]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-04-04]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [object]
     */

    public function queryNotFound()
    {
        $this->factoryPage('error/erroPaginator');
    }
}
