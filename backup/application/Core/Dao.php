<?php

namespace SistemaPro\Core;

use SistemaPro\Core\Abstracts\DaoAbstract;

class Dao extends DaoAbstract
{
    private $field = array();
    private $table;
    private $query;

    /**
     * @method   [setTable] [Responsável por setar o nome da entidade do banco]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [String]
     */

    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @method   [save] [Método que salva os dados na entidade coluna/valor]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [array $dados]
     * @return   [String]
     */

    public function save($dados)
    {
        $this->filterSave($dados);

        if(!isset($dados['id']) || !$dados['id']) {
            $this->db->query("INSERT INTO {$this->table} (" . implode(', ', array_keys($dados)) . ") 
                VALUES ('" . implode("','", $dados) . "')"
            );
            
            return $this->db->lastInsertId();
        
        } else {
            
            $id = $dados['id'];
            unset($dados['id']);
            $last = count($dados) -1;
            $set = '';
            $c = 0;
            
            foreach($dados as $campo => $valor) {
                if($c == $last)
                   $set .= "{$campo} = '{$valor}'";
                else
                   $set .= "{$campo} = '{$valor}' , ";
                $c++;
            }

            return $this->db->query("UPDATE {$this->table} SET {$set} WHERE id={$id}");
        }
    }

    /**
     * @method   [delete] [Deleta dados na entidade do banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [int $id]
     * @return   [String]
     */

    public function delete($id)
    {
        return $this->db->query("DELETE FROM {$this->table} WHERE id={$id}");
    }

    /**
     * @method   [simpleQuery] [Executa dados na entidade do banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Array]
     */

    public function simpleQuery()
    {
        return $this->db->query("SELECT * FROM {$this->table}");
    }

    /**
     * @method   [countQuery] [Retorna total de dados na entidade do banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-16-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Array]
     */

    public function countQuery()
    {
        return $this->db->query("SELECT COUNT(*) AS total FROM {$this->table}");
    }

    /**
     * @method   [fetchAll] [Retorna array de dados de toda entidade do banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-11-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [String $recordset, array $values]
     * @return   [Array]
     */

    public function fetchAll($recordset, array $values)
    {
        $resultSet = array();

        while($row = $recordset->fetch(\PDO::FETCH_OBJ)){ 
            $result = array();
            
            foreach ($values as $retorno => $coluna) {
                if (property_exists($row, $coluna)) {
                    $result[$retorno] = $row->{$coluna};
                }                
            }

            $resultSet[] = $result;
        }

        return $resultSet;
    }

    /**
     * @method   [fetchRow] [Retorna dados de toda entidade do banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [int $id]
     * @return   [Array]
     */

    public function fetchRow($id)
    {
        return $this->db->query("SELECT * FROM {$this->table} WHERE id = {$id}");
    }

    /**
     * @method   [query] [Cria uma query personalizada]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [String $query]
     * @return   [String]
     */

    public function query($query)
    {
        return $this->db->query($query);
    }

    /**
     * @method   [filterSave] [Filtro para tipagem de array nas queries]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [Array $dados]
     * @return   [void]
     */

    private function filterSave($dados)
    {
        if(!is_array($dados))
            die("Dados para insercao e edicao devem ser do tipo array");

    }

    /**
     * @method   [setQuery]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao] [setters]
     * @version  [1.0.2]
     * @param    [$query]
     * @return   [void]
     */

    protected function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @method   [getQuery]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @return   [PDO]
     */

    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @method   [setField]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao] [setters]
     * @version  [1.0.2]
     * @param    [$field]
     * @return   [void]
     */

    protected function setField(array $field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @method   [getField]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @return   [PDO]
     */

    public function getField()
    {
        return $this->field;
    }
}
