<?php

namespace SistemaPro\Core;

use Zend\ServiceManager\ServiceManager;

class Service extends ServiceManager
{
    /**
     * @method   [getServiceLocator] [Metodo que faz abstração do service manager do ZF2]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Object]
     * @see      Zend\ServiceManager\ServiceManager
     */
    
    public function getServiceLocator()
    {
        return $this->creationContext;
    }
}