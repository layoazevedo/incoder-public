<?php

namespace SistemaPro\Core;

use SistemaPro\Utils\Url;
use SistemaPro\Servlet\GetRequest;
use SistemaPro\Servlet\PostRequest;
use SistemaPro\Servlet\RouterResponse;
use SistemaPro\Core\Abstracts\RouterAbstract;
use SistemaPro\Core\Interfaces\ExecRouterInterface;
use SistemaPro\Core\Interfaces\AdminRouterInterface;
use SistemaPro\Core\Interfaces\DefaultRouterInterface;

class Router extends RouterAbstract implements ExecRouterInterface
{
    protected $path = array();
    protected $routerConfig;
    protected $url;
    protected $explode;
    protected $module;
    protected $controller;
    protected $action;
    protected $paramUrl;

    public function __construct()
    {
        self::getError();

        $this->getUrlFromServer();
        $this->setPath();
        $this->setRouterConfig();
    }

    /**
     * @method   [run] [Retorna instancia do objeto module, controller e action]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [void]
     * @see      RouterAbstract::getError()
     * @throws   Instancia de objeto nao criada
     */

    public function run()
    {
        $fullQualifiedClassName = 'SistemaPro\\Module\\'.$this->getModule().'\\Controller\\' . $this->getController();

        try {

            if (class_exists($fullQualifiedClassName)) {
                $RouterHttp = (new RouterResponse())
                    ->setRouterHttp($this->explode);

                $post = (new PostRequest())
                    ->setPostHttp($_POST);

                $get =  (new GetRequest())
                    ->setGetHttp($_GET);

                $controller = (new $fullQualifiedClassName())
                    ->setRouter($this)
                    ->setPostRequestHttp($post)
                    ->setGetRequestHttp($get)
                    ->setRouterHttp($RouterHttp)
                    ->setAmbiente(new Url())
                    ->init();

                if (method_exists($fullQualifiedClassName,$this->getAction())) {
                    $controller->{$this->getAction()}();
                }
            }

        } catch (\Exception $e) {
            echo 'Instancia de objeto nao criada:'. $e->getMessage();
        }

    }

    /**
     * @method   [getUrlFromServer] [Verifica se a chave do request esta vazia]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [void]
     */

    protected function getUrlFromServer()
    {
        $this->url = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'].'/' : 'main';
    }

    /**
     * @method   [setPath] [Verifica o roteamento esta vazio e injeta o router]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [void]
     */

    protected function setPath()
    {
        $routerDefault = array(
            DefaultRouterInterface::MODULE,
            DefaultRouterInterface::CONTROLLER,
            DefaultRouterInterface::ACTION
        );

        if ($this->url == DefaultRouterInterface::MODULE) {
            $this->setConfigRouter($routerDefault[0], $routerDefault[1], $routerDefault[2]);
        } else {
            $this->explode = array_values(array_filter(explode ('/', $this->url)));
        }
    }

    /**
     * @method   [setRouterConfig] [Verifica se o roteamento é via admin ou arquivo de config]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [void]
     * @see      Config/module.config.php (Arquivo de configuração de rotas)
     */

    private function setRouterConfig()
    {
        $this->moduleConfig = require dirname(__FILE__) . '/../Config/module.config.php';
        $config = $this->moduleConfig['routerConfig']['router'];

        if ($this->explode[0] == AdminRouterInterface::CRUD) {

            if (isset($this->explode[0]) && isset($this->explode[1])
                && isset($this->explode[2]) && ($this->explode[2] != '')) {

                $this->setConfigRouter($this->explode[0], $this->explode[1], $this->explode[2]);

                return $this;
            }
        }
    }

    /**
     * @method   [setConfigRouter] [Faz a injeção de todas as configurações da rota]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [$module, $controller, $action]
     * @return   [void]
     * @see      Config/module.config.php (Arquivo de configuração de rotas)
     */

    private function setConfigRouter($module, $controller, $action)
    {
        $this->setModule($module);
        $this->setController($controller);
        $this->setAction($action);
    }

    /**
     * @method   [setModule]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [setters]
     * @version  [1.0.2]
     * @param    [$module]
     * @return   [void]
     */

    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @method   [getModule]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @return   [String]
     */

    public function getModule()
    {
        return ucfirst($this->module);
    }

    /**
     * @method   [setController]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [setters]
     * @version  [1.0.2]
     * @param    [$controller]
     * @return   [void]
     */

    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @method   [getController]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @return   [String]
     */

    public function getController()
    {
        return  ucfirst($this->controller) . AdminRouterInterface::CONTROLLER;
    }

    /**
     * @method   [setAction]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [setters]
     * @version  [1.0.2]
     * @param    [$action]
     * @return   [void]
     */

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

     /**
     * @method   [getAction]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @return   [String]
     */

    public function getAction()
    {
        return $this->action . AdminRouterInterface::ACTION;
    }
}
