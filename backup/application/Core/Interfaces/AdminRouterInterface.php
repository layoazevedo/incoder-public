<?php

namespace SistemaPro\Core\Interfaces;

interface AdminRouterInterface
{
    /**
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Interfaces]
     * @since    [2016-09-03]
     * @category [Interface]
     * @version  [1.0.2]
     */

    const MAIN            = 'Main';
    const MODULE          = 'Admin';
    const MODULE_PESQUISA = 'Pesquisa';
    const MODULE_TERMO    = 'Termos';
    const CONTROLLER      = 'Controller';
    const ACTION          = 'Action';
    const CRUD            = 'admin';
    const PESQUISA        = 'pesquisa';
    const TERMO           = 'termos';
}
