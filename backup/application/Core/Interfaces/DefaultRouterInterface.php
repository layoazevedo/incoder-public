<?php

namespace SistemaPro\Core\Interfaces;

interface DefaultRouterInterface
{
    /**
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Interfaces]
     * @since    [2016-09-03]
     * @category [Interface]
     * @version  [1.0.2]
     */
    
    const MODULE = 'main';
    const CONTROLLER = 'home';
    const ACTION = 'index';
}
