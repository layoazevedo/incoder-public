<?php

namespace SistemaPro\Core\Interfaces;

interface ServiceInterface
{
    /**
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Interfaces]
     * @since    [2016-11-03]
     * @category [Interface]
     * @version  [1.0.2]
     */

    const AMBIENTE        = 'getAmbiente';
    const CONTROLLER_VIEW = 'getController';
    const MENU            = 'menu';
    const PAGINA          = 'pagina';
}
