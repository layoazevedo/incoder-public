<?php

namespace SistemaPro\Core;

class Request
{
    private $request;
    
    /**
     * @method   [isPost] [Verifica o request]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-06-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [object]
     * @throws   Router nao setado
     */
    
    protected function isPost()
    {
        if(isset($_POST) && !empty($_POST))
            return true;
        return false;
    }

    /**
     * @method   [getPost] [Recupera o valor do post por chave]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-06-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [$key]
     * @return   [Array mixed]
     */
    
    protected function getPost($key)
    {
        return $_POST[$key];
    }
}