<?php

namespace SistemaPro\Core\Abstracts;

use SistemaPro\Utils\Url;
use SistemaPro\Core\Router;

abstract class ControllerAbstract
{
    protected $post = array();
    protected $router;
    protected $helper;
    protected $ambiente;

    /**
     * @method   [setRouter] [Responsável por setar o router]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-06-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [Router $router]
     */

    public function setRouter(Router $router)
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @method   [getRouter] [Retorna instancia do objeto]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-06-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [object]
     * @throws   Router nao setado
     */

    public function getRouter()
    {
        if (!$this->router instanceof Router) {
            throw new \Exception("Router nao setado");
        }
        return $this->router;
    }

    /**
     * @method   [setAmbiente] [Responsável por setar o ambiente]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-19-04]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [$ambiente, Url]
     */

    public function setAmbiente(Url $ambiente)
    {
        $this->ambiente = $ambiente;
        return $this;
    }

    /**
     * @method   [getAmbiente] [Retorna instancia do objeto]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-19-04]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [object]
     * @throws   Helper nao setada
     */

    public function getUrlEnv()
    {
        if (!$this->ambiente instanceof Url) {
            throw new \Exception("Ambiente nao setado");
        }
        return $this->ambiente;
    }

    /**
     * @method   [pageNotFound]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @throws   [Exception] [Erro: 404 pagina nao encontrada]
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-30-03]
     * @category [Controller]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [null]
     */

    public function pageNotFound()
    {
        throw new \Exception("Erro: 404 pagina nao encontrada");
    }
}
