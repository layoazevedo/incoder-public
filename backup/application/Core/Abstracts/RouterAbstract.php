<?php

namespace SistemaPro\Core\Abstracts;

abstract class RouterAbstract
{
    protected $defaultController;
    protected $defaultAction;
    protected $defaultModule;
    protected $router;

    /**
     * @method   [getError] [Verifica se existe o env SISTEMA_PRO_ENV e retorna os erros]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-06-03]
     * @category [Router]
     * @version  [1.0.2]
     * @param    [null]
     */
    
    protected static function getError()
    {
        if (isset($_SERVER['SISTEMA_PRO_ENV']) && $_SERVER['SISTEMA_PRO_ENV'] != 'production') {
            ini_set('display_errors',1);
            ini_set('display_startup_erros',1);
            error_reporting(E_ALL);
        }
    }

    /**
     * @method   [setDefaultController]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-09-03]
     * @category [Router] [setters]
     * @version  [1.0.2]
     * @param    [$defaultController]
     * @return   [void]
     */
    
    public function setDefaultController($defaultController)
    {
        $this->defaultController = $defaultController;
        return $this;
    }

    /**
     * @method   [getDefaultController]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [String]
     */

    public function getDefaultController()
    {
        return $this->defaultController;
    }

    /**
     * @method   [setDefaultAction]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-09-03]
     * @category [Router] [setters]
     * @version  [1.0.2]
     * @param    [$defaultAction]
     * @return   [void]
     */

    public function setDefaultAction($defaultAction)
    {
        $this->defaultAction = $defaultAction;
        return $this;
    }

    /**
     * @method   [getDefaultAction]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core\Abstracts]
     * @since    [2016-09-03]
     * @category [Router] [getters]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [String]
     */

    public function getDefaultAction()
    {
        return $this->defaultAction;
    }
}
