<?php

namespace SistemaPro\Core\Abstracts;

use SistemaPro\Utils\Url;

abstract class DaoAbstract
{
    private $ambiente;
    protected $db ;

    /**
     * @method   [construct] [Retorna a conexao com o banco de dados]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-02-03]
     * @category [Dao]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [array Mixed]
     * @see      Config/dao.config.php
     */

    public function __construct()
    {
        $this->setAmbiente('production');

        $database = require dirname(__FILE__) . '/../../Config/dao.config.php';

        $routerUrl = new Url();
        $config = $database['daoConfig']['connection']['production'];

        $this->db = new \PDO("mysql:host={$config['hostname']};dbname={$config['dbname']}", $config['username'], $config['password'],
            array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            )
        );
    }

    /**
     * @method   [setAmbiente]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-22-03]
     * @category [Dao] [getters]
     * @version  [1.0.2]
     * @return   [null]
     */

    public function setAmbiente($ambiente)
    {
        $this->ambiente = $ambiente;
        return $this;
    }

    /**
     * @method   [getAmbiente]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Core]
     * @since    [2016-22-03]
     * @category [Dao] [getters]
     * @version  [1.0.2]
     * @return   [Object]
     */

    public function getAmbiente()
    {
        return $this->ambiente;
    }
}
