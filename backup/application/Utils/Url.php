<?php

namespace SistemaPro\Utils;

class Url
{
    private $url;

     /**
     * @method   [verificaRouter] [Verifica o tipo do router]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Utils]
     * @since    [2016-23-03]
     * @category [Url]
     * @version  [1.0.2]
     * @param    [$router]
     * @return   [bool]
     */

    public function verificaRouter($router)
    {
        $url = explode('/', @$_SERVER['REDIRECT_URL']);

        if (in_array($router, $url)) {
            return true;
        }

        return false;
    }

    /**
     * @method   [getAmbiente] [Gera url atual conforme ambiente setado]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Utils]
     * @since    [2016-23-03]
     * @category [Url]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [String]
     */

    public function getAmbiente()
    {
        return $_SERVER['HTTP_HOST'];
    }

    /**
     * @method   [getEnv] [Tras o valor da env no hosts]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @package  [SistemaPro\Utils]
     * @since    [2016-23-03]
     * @category [Url]
     * @version  [1.0.2]
     * @param    [null]
     * @return   [Array mixed]
     */

    private function getEnv()
    {
        return $_SERVER['GANESHA_IT_ENV'];
    }
}
