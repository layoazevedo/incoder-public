<?php

namespace SistemaPro\Utils;

class Count
{
    public function getValue(array $value)
    {
        if (!is_array($value)) {
            throw new \Exception("Erro: valor tem que ser um array");
        }

        return count($value);
    }
}