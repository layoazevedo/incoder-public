<?php

namespace SistemaPro\Utils;

class String
{
    public function getStringFormat($value)
    {
        return ucwords(strtolower(str_replace("_"," ",str_replace("-"," ",$value))));
    }
}