<?php

namespace SistemaPro\Utils;

class Security
{
    private $value;

    public function getSecurity($value)
    {
       return htmlentities(addslashes(strip_tags($value)));
    }

    public function getMd5($value)
    {
        return md5($value);
    }
}