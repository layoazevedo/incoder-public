<?php

/**
 * @author   [Layo Demetrio] <layoazevedo@gmail.com>
 * @package  [SistemaPro\Config]
 * @since    [2016-07-03]
 * @category [DaoConfig]
 * @version  [1.0.2]
 * @return   [Array mixed]
 */

return array(
    'daoConfig' => array(
        'connection' =>  array(
            'production' => array(
                'hostname' => 'gcsdesenv.mysql.dbaas.com.br',
                'username' => 'gcsdesenv',
                'password' => 'bdgcsdesenv*08',
                'dbname'   => 'gcsdesenv',
            ),
        ),
    ),
);
