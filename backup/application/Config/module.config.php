<?php

/**
 * @author   [Layo Demetrio] <layoazevedo@gmail.com>
 * @package  [SistemaPro\Config]
 * @since    [2016-07-03]
 * @category [RouterConfig]
 * @version  [1.0.2]
 * @return   [Array mixed]
 */

return array(
    'routerConfig' => array(
        'router' =>  array(
            '/' => array(
                'module' => 'Main',
                'controller' => 'Main\Controller',
                'action' => 'HomeController',
                'child' => false,
            ),
        ),
    ),
);
