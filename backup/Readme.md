
 * @author   [Layo Demetrio] <layoazevedo@gmail.com>
 * @package  [SistemaPro\Config]
 * @since    [2016-23-03]
 * @category [SistemaPro]
 * @version  [1.0.2]


# Instruções de uso

# Framework
- O framework sistema pró é baseado em MVC e que tem como finalidade trabalhar em diversas camadas e rotas.

# Dependencias
- "twig/twig": "~1.0"
- "zendframework/zend-servicemanager": "*"

# Padrão FIG
- PSR-4

# Versão
- PHP 5.4+

# Dao
- As conexões iniciais da camada de Dao estão divididas em:
    - development
    - production

- Arquivos:
    - Config/dao.config.php
    - Core/Abstracts/DaoAbstract.php

- Método de configuração:
    - $this->setAmbiente('ambiente');

# Router
- Existem dois tipos de router no framework:

- Admin: Baseado em Module/Controller/Action.
- Mapas: Configurado por array de configuração.
