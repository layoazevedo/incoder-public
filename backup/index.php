<?php

/**
 * @author    [Layo Demetrio] <layoazevedo@gmail.com>
 * @package   [SistemaPro]
 * @since     [2016-02-03]
 * @category  [Main]
 * @version   [1.0.2]
 */

require "vendor/autoload.php";

$application = new \SistemaPro\Core\Router();
$application->run();