<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- ESTILO PRINCIPAL -->
        <link rel="stylesheet" href="assets/style.css" type="text/css" />
        <link rel="stylesheet" href="assets/style-responsive.css" type="text/css" />
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <!-- JQUERY -->
        <script src="http://code.jquery.com/jquery-1.12.4.js"></script>
  		<script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <!-- BOOTSTRAP -->
        <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
        <!-- SCROLL -->
        <link rel="stylesheet" href="assets/scroll/dist/optiscroll.css">
        <script type="text/javascript" src="assets/scroll/src/polyfills/customevent.polyfill.js"></script>
        <script type="text/javascript" src="assets/scroll/src/polyfills/raf.polyfill.js"></script>
        <script type="text/javascript" src="assets/scroll/src/optiscroll.js"></script>
        <script type="text/javascript" src="assets/scroll/src/events.js"></script>
        <script type="text/javascript" src="assets/scroll/src/scrollbar.js"></script>
        <script type="text/javascript" src="assets/scroll/src/utils.js"></script>
        <script type="text/javascript" src="assets/scroll/src/globals.js"></script>
	</head>
