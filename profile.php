<?php
require "includes/db/conn.php";
require "includes/header.php";
require "includes/menu-superior.php";
require "includes/menu-lateral.php";

$userById = mysqli_query($conn, "SELECT * FROM Tab_CadGeral where ID_EFETIVO = '".$_GET['account']."'");

while ($obj = $userById->fetch_object()) {
    $nomeColaborador     = utf8_encode($obj->NOME_COLABORADOR);
    $foto                = utf8_encode($obj->CAMINHO_FOTO);
    $idEfetivo           = utf8_encode($obj->ID_EFETIVO);
    $enderecoResidencial = utf8_encode($obj->ENDERECO_RESIDENCIAL);
    $cidade              = utf8_encode($obj->CIDADE_NASC);
    $cep                 = utf8_encode($obj->CEP);
    $telefoneResidencial = utf8_encode($obj->TELEFONE_RESIDENCIAL);
    $telefoneCelular     = utf8_encode($obj->TELEFONE_CELULAR);
    $email               = utf8_encode($obj->E_MAIL);
    $cargo               = utf8_encode($obj->CARGO);
    $status              = utf8_encode($obj->STATUS);
    $dataAdmissao        = utf8_encode($obj->DATA_DE_ADMISSAO);
    $matricula           = utf8_encode($obj->MATRICULA);
}

$notas = mysqli_query($conn, "SELECT * FROM Tab_EntrevNotas where ID_EFETIVO = '".$_GET['account']."'");

$treinamento = mysqli_query($conn, "
SELECT
    DATE(CADASTRO) as cadastro,
    EXAM_ONLINE as exameOnline,
    CONC_PERC as percurso,
    TIPO_CURSO as tipoCurso,
    NOME_CURSO as nomeCurso
FROM
    Tab_Treinamentos
WHERE
    ID_EFETIVO = '".$_GET['account']."'");

?>

!-- MENU CONTEÚDO -->
<section class="conteudo">
	<div class="container">
    	<div id="conteudo" class="conteudo padding-left-255">
            <div class="row">
            	<h1>
                	<a href="/"><i class="fa fa-arrow-circle-o-left return"></i></a>
                	EMPLOYEE <font color="#2f51a5"><?php echo $nomeColaborador ?></font>
                </h1>

                <form>
                    <div class="col-md-3">
                    	<div class="foto-perfil">
                            <?php
                                if(empty($foto)) {
                                    echo '<img src="http://gcsportoseguro.com.br/photos/sem-foto.jpg">';
                                } else {
                                    echo '<img src="http://gcsportoseguro.com.br/'.$foto.'">';
                                }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                    	<div class="row">
                            <div class="col-md-5">
                                <label>First Name</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $nomeColaborador ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Last Name</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $nomeColaborador ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Prefix</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $idEfetivo ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Title</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $nomeColaborador ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Adress</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $enderecoResidencial ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>City</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $cidade ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>State</label>
                            </div>
                            <div class="col-md-7">
                            	<select>
                                	<option value="<?php echo $cidade ?>"><?php echo $cidade ?></option>
                                </select>
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Zipcode</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $cep ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Phone</label>
                            </div>
                            <div class="col-md-7">
                                <input value="<?php echo $telefoneResidencial ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Mobile</label>
                            </div>
                            <div class="col-md-7">
                                <input class="cell" value="<?php echo $telefoneCelular ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>E-mail</label>
                            </div>
                            <div class="col-md-7">
                                <input class="mail" value="<?php echo $email ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Skype</label>
                            </div>
                            <div class="col-md-7">
                                <input class="skype">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Department</label>
                            </div>
                            <div class="col-md-7">
                            	<select>
                                	<option value="<?php echo $cargo ?>"><?php echo $cargo ?></option>
                                </select>
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Status</label>
                            </div>
                            <div class="col-md-7">
                            	<select>
                                	<option value="<?php echo $status ?>"><?php echo $status ?></option>
                                </select>
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>Hire Date</label>
                            </div>
                            <div class="col-md-7">
								<input value="<?php echo $dataAdmissao ?>">
                            </div>
                        </div>
                    	<div class="row">
                            <div class="col-md-5">
                                <label>DOE</label>
                            </div>
                            <div class="col-md-7">
								<input value="<?php echo $matricula ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    	<div class="row">
                            <div class="col-md-12">
                                <textarea placeholder="About"></textarea>
                            </div>
                        </div>
                    </div>
        		</form>

                <div class="col-md-12">
               		<div class="table-responsive tabela-dados">
                    	<table class="table table-bordered table-hover">
							<thead>
                            	<tr>
                              		<th>Due Date</th>
                              		<th>% Complete</th>
                              		<th>Assigned to</th>
                              		<th>Owned by</th>
                              		<th>Subject</th>
                            	</tr>
                          	</thead>
                          	<tbody>
								<?php
                                    while ($obj = $treinamento->fetch_object()) {
                                        echo '<tr>
    	                                	<td>'.$obj->cadastro.'</td>
    	                                    <td>
    	                                    	<div class="barra" style="width:'.$obj->exameOnline.'%;"></div>
    	                                        <span>'.$obj->exameOnline.'%</span>
    	                                   	</td>
    	                                    <td>'.utf8_encode($obj->percurso).'</td>
    	                                    <td>'.utf8_encode($obj->tipoCurso).'</td>
    	                                    <td>'.utf8_encode($obj->nomeCurso).'</td>
    	                            	</tr>';
                                    }

                                    while ($obj = $notas->fetch_object()) {
                                        echo '<tr>
    	                                	<td>--</td>
    	                                    <td>
    	                                    	<div class="barra" style="width:'.$obj->MEDIA.'%;"></div>
    	                                        <span>'.$obj->MEDIA.'%</span>
    	                                   	</td>
    	                                    <td>--</td>
    	                                    <td>'.$obj->Tipo.'</td>
    	                                    <td>--</td>
    	                            	</tr>';
                                    }
                                ?>
							</tbody>
                        </table>
                	</div>
                </div>

                <div class="col-md-12">
                    <button type="button" class="botao" data-toggle="collapse" data-target="#graphics">Graphics</button>
                    <div id="graphics" class="collapse">
                        <div class="row">
                        	<div class="col-md-3">
                                Gráfico 1
                            </div>
                            <div class="col-md-3">
                                Gráfico 2
                            </div>
                            <div class="col-md-3">
                                Gráfico 3
                            </div>
                            <div class="col-md-3">
                                Gráfico 4
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
        </div>
	</div>
</section>

<!-- JQUERY SIDEBAR E TOOLTIP -->
<script>
	$.sidebarMenu($('.sidebar-menu'));

	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

<script>
$(document).ready(function(){
	$(".toggle").click(function(){
		$(".menu-lateral").toggle('slide',200);
	});
	$(".toggle").click(function(){
		$("#conteudo").toggleClass('padding-left-235 padding-left-0');
	});
});
</script>

<script type="text/javascript">
	var wr = new Optiscroll(document.getElementById('m-wrapper'), { forceScrollbars: true });
	var wr = new Optiscroll(document.getElementById('m-wrapper-top'), { forceScrollbars: true });
</script>
</body>
</html>
